package org.artyom.readwrite;

import java.io.*;
import java.util.*;


public class Reader {

    static File fileToRead; // файл, содержимое которого необходимо считать
    static String fileContent = ""; // содержимое файла для чтения и последующей записи

    // метод выбора файла для чтения
    public static void choosingFileToRead() {
        System.out.println("Введите путь к файлу для чтения");
        Scanner scanner = new Scanner(System.in);
        fileToRead = new File(scanner.nextLine());
    }

    // проверка существования файла для чтения
    public static boolean ifFileToReadExists() {
        if (fileToRead.exists()) {return true;}
        else {return false;}
    }

    // чтение содержимого файла. результат присваивается ссылке fileContent
    public void read() {
        try {
            BufferedReader fileReader = new BufferedReader(new FileReader(fileToRead));
            String line;

            while ((line = fileReader.readLine()) != null) {
                fileContent += line;
            }

            fileReader.close();
        }

        catch (IOException e)
        {
            System.out.println("Ошибка чтения файла");
        }
    }
}
