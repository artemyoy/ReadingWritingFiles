package org.artyom.readwrite;

import java.io.*;
import java.util.*;


public class Writer {

    static String userInput; // для ввода имени файла, в который будет записываться информация
    static File fileToWrite; // файл, в который будет записываться информация

    // выбираем файл для записи
    public static void choosingFileToWrite() {
        Scanner scanner = new Scanner(System.in);
        userInput = scanner.nextLine();
        fileToWrite = new File(Writer.userInput);
    }

    // проверка существования файла
    public static boolean ifFileToWriteExists() {
        if (fileToWrite.exists()) {return true;}
        else {return false;}
    }

    // создание файла для записи в него информации
    public static void fileCreator() {
        //fileToWrite = new File(Writer.userInput);
        try {
            fileToWrite.createNewFile();
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    // запись информации в файл
    public void writeContent() {
        try {
            //BufferedWriter fileWriter = new BufferedWriter(new FileWriter(Writer.fileToWrite));
            FileWriter fileWriter = new FileWriter(Writer.fileToWrite, true);
            fileWriter.append(Reader.fileContent);
            Reader.fileContent = "";
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Ошибка данных");
        }
    }
}