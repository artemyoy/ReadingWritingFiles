package org.artyom.readwrite;


import java.util.*;


public class Main {

    public static void main(String[] args) {

        System.out.println("Введите выходной файл:");

        // выбор файла для записи, проверка его существования и создание нового файла в случае его отсутствия
        Writer.choosingFileToWrite();
        if (! Writer.ifFileToWriteExists()) {
            Writer.fileCreator();
        }

        Main theApp = new Main();
        theApp.runTheApp();
    }

    public void runTheApp() {

        try {
            System.out.println("Хотите считать файл? Нажмите \"1\", чтобы продолжить. Нажмите \"2\" для выхода.");
            Scanner scanner = new Scanner(System.in);
            int choice = scanner.nextInt();
            if (choice == 2) {
                System.out.println("До свидания");
                return;
            } else if (choice != 1) {
                Main main = new Main();
                main.runTheApp();
            }
        } catch (Exception e) {
            System.out.println("неверный ввод");
            Main main = new Main();
            main.runTheApp();
        }

        Reader.choosingFileToRead();
        while (! Reader.ifFileToReadExists()) {
            Reader.choosingFileToRead();
            if (Reader.ifFileToReadExists()) break;
        }

        Reader reader = new Reader();
        reader.read();
        System.out.println("Следующая информация будет записана в файл:\n" + Reader.fileContent);

        Writer writer = new Writer();
        writer.writeContent();
        System.out.println("Данные записаны");

        Main main = new Main();
        main.runTheApp();
    }
}
